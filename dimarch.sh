#!/bin/zsh

# Mainly https://itsfoss.com/install-arch-linux/
# secondary: https://wiki.archlinux.org/title/Installation_guide

### Validations
ls /sys/firmware/efi/efivars
ip link
ping archlinux.org

### Seting the time
timedatectl set-ntp true

### Partitioning
fdisk -l
fdisk /dev/sda

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
g #create new table
n #new partition
1 #partition ID
[enter] #choose default (first) startign sector
t #change the partition type
uefi #use the uefi alias
n
2
[enter]

v #validate
w # save

# FS formatting
mkfs.fat -F 32 /dev/sda1
mkfs.ext4 /dev/sda2

# partition mounting
mkdir /mnt/efi
mount /dev/sda1 /mnt/efi
mount /dev/sda2 /mnt




# Install everything
pacstrap /mnt base linux linux-firmware vim nano iwd man-db man-pages texinfo firefox. 
genfstab -U /mnt >> /mnt/etc/fstab # Generate fstab

arch-chroot /mnt
#Bootlaoader - GRUB
pacman -S grub efibootmgr --noconfirm
mkdir /boot/efi
mount /dev/sda1 /boot/efi
systemctl daemon-reload
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg


echo "dimarch" >> /etc/hostname


echo -e "127.0.0.1	localhost\n::1		localhost\n127.0.1.1	dimarch" >> /etc/hosts


timedatectl set-timezone Asia/Jerusalem
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=de-latin1" >> /etc/vconsole.conf

# Desktop - xorg+gnome
pacman -S xorg gnome networkmanager --noconfirm

systemctl start gdm.service
systemctl enable gdm.service
systemctl enable NetworkManager.service

exit
shutdown now








